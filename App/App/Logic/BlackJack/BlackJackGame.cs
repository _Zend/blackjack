﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.Entities.BlackJack;
using App.Factories.BlackJack;
using App.Factories.Util;
using App.Interfaces;
using App.Interfaces.Util;
using App.Logic.Util;

namespace App.Logic.BlackJack
{
    public class BlackJackGame : IGameLogic
    {
        private bool IsGameLocked { get; set; }

        private bool IsGameStopped { get; set; }

        private List<AbstractPlayer> Players { get; set; }

        private Dictionary<int, List<Statistics>> RoundStats { get; set; }

        private AbstractPlayer UserPlayer { get; set; }

        private IScorer Scorer { get; set; }

        private IMediator Mediator { get; set; }

        private int CurrentRound { get; set; } = 1;

        public OperationResult StartGame()
        {
            if (Players.Count < 2)
            {
                return new OperationResult()
                {
                    Success = false,
                    Message = "It requires at least 2 players to begin the game."
                };
            }

            Mediator.Log("Type the name of the player to play for: ", Players.Select(t => t.Name).ToList());
            var response = Mediator.Recieve();
            UserPlayer = Players.Find(t => t.Name.ToLower() == response.ToLower());

            Mediator.Log("The game started.");

            while (!IsGameStopped)
            {
                IsGameLocked = true;
                StartRound();
                CurrentRound++;
            }

            return new OperationResult() {Success = true};
        }

        public OperationResult FinishGame()
        {
            IsGameStopped = true;
            Mediator.Log("The game is finished.");

            return new OperationResult() {Success = true};
        }

        public OperationResult StartRound()
        {
            Mediator.Log("New round started.");
            IsGameLocked = true;

            var deck = new DeckFactory().CreateDeck();
            var playersCards = GiveInitialCards(deck, Players);
            var playersScores = new Dictionary<AbstractPlayer, int>();

            foreach (var player in Players.OrderBy(t => t.IsDealer))
            {
                playersScores.Add(player, 0);
                GetPlayerScores(playersCards, playersScores, player);

                if (player == UserPlayer)
                {
                    while (true)
                    {
                        GetPlayerScores(playersCards, playersScores, player);
                        var playerCards = GetPlayerCards(playersCards, player);
                        if (playersCards[player].Count > 4)
                        {
                            Mediator.Log(
                                $"Your cards: {playerCards}, your score: {playersScores[player]}.\nYou cant take more cards.");
                            break;
                        }

                        Mediator.Log(
                            $"Your cards: {playerCards}, your score: {playersScores[player]}.\nDo you wish to take a card?");
                        var response = Convert.ToBoolean(Mediator.Recieve());
                        if (response)
                        {
                            playersCards[player].Add(deck.GetRandomCard());
                            continue;
                        }
                        break;
                    }
                    continue;
                }

                if (new Random().Next(10) > 5)
                {
                    playersCards[player].Add(deck.GetRandomCard());
                    Mediator.Log($"Player {player.Name} takes a card.");
                }
                else
                {
                    Mediator.Log($"Player {player.Name} doesnt take a card.");
                }
            }

            var winners = GetWinners(playersScores);
            Mediator.Log("Winners:", winners.Select(t => t.Name).ToList());
            UpdateStatistics(winners);

            FinishRound();

            return new OperationResult() {Success = true};
        }

        private OperationResult UpdateStatistics(List<AbstractPlayer> winners)
        {
            RoundStats.Add(CurrentRound, new List<Statistics>());
            foreach (var player in Players)
            {
                if (RoundStats.Keys.Count > 1)
                {
                    var previousRoundKey = RoundStats.Keys.First(t => t == CurrentRound - 1);
                    var previousRoundPlayerStats = RoundStats[previousRoundKey].FirstOrDefault(t => t.Player == player);

                    var currentRoundStats = new Statistics();
                    if (previousRoundPlayerStats != null)
                    {
                        currentRoundStats.Player = player;
                        if (winners.Contains(player))
                        {
                            currentRoundStats.Wins = previousRoundPlayerStats.Wins + 1;
                            currentRoundStats.Losses = previousRoundPlayerStats.Losses;
                        }
                        else
                        {
                            currentRoundStats.Wins = previousRoundPlayerStats.Wins;
                            currentRoundStats.Losses = previousRoundPlayerStats.Losses + 1;
                        }
                        currentRoundStats.Total = previousRoundPlayerStats.Total + 1;
                    }
                    else
                    {
                        currentRoundStats.Player = player;
                        if (winners.Contains(player))
                        {
                            currentRoundStats.Wins = 1;
                            currentRoundStats.Losses = 0;
                        }
                        else
                        {
                            currentRoundStats.Wins = 0;
                            currentRoundStats.Losses = 1;
                        }
                        currentRoundStats.Total = 1;
                    }
                    RoundStats[CurrentRound].Add(currentRoundStats);
                }
                else
                {
                    var firstRoundKey = RoundStats.Keys.First();
                    var currentRoundStats = new Statistics {Player = player};
                    if (winners.Contains(player))
                    {
                        currentRoundStats.Wins = 1;
                    }
                    else
                    {
                        currentRoundStats.Losses = 1;
                    }
                    currentRoundStats.Total = 1;

                    RoundStats[firstRoundKey].Add(currentRoundStats);
                }
            }
            return new OperationResult() {Success = true};
        }

        private List<AbstractPlayer> GetWinners(Dictionary<AbstractPlayer, int> playersScores)
        {
            var dealer = playersScores.Keys.First(t => t.IsDealer);
            var winners = new List<AbstractPlayer>();
            var isDealerWinning = false;

            foreach (var playersScore in playersScores)
            {
                if (playersScore.Key == dealer)
                {
                    continue;
                }

                if (playersScore.Value > playersScores[dealer] && playersScore.Value < 22)
                {
                    winners.Add(playersScore.Key);
                    continue;
                }

                isDealerWinning = true;
            }

            if (isDealerWinning)
            {
                winners.Add(dealer);
            }

            return winners;
        }

        private Dictionary<AbstractPlayer, List<Card>> GiveInitialCards(Deck deck, List<AbstractPlayer> players)
        {
            if (deck.Cards.Count == 0)
            {
                throw new Exception("Deck is empty. Not all players gained their cards.");
            }

            var playersCards = new Dictionary<AbstractPlayer, List<Card>>();
            foreach (var player in players.OrderBy(t => t.IsDealer))
            {
                var cardsList = new List<Card>();

                for (var i = 0; i < 2; i++)
                {
                    cardsList.Add(deck.GetRandomCard());
                }

                playersCards.Add(player, cardsList);
            }

            return playersCards;
        }

        public OperationResult FinishRound()
        {
            Mediator.Log("Round finished.");
            Mediator.Log("Results:");
            foreach (var stats in RoundStats[CurrentRound])
            {
                Mediator.Log($"{stats.Player.Name} W:L:T ");
                Mediator.Log($"{stats.Wins}:{stats.Losses}:{stats.Total}");
            }
            IsGameLocked = false;

            while (true)
            {
                Mediator.Log("Do you want to stop the game?");
                var stopResponse = Convert.ToBoolean(Mediator.Recieve());
                if (stopResponse)
                {
                    FinishGame();
                    return new OperationResult() {Success = true};
                }

                Mediator.Log("Do you want to add a player?");
                var addPlayerResponse = Convert.ToBoolean(Mediator.Recieve());
                if (addPlayerResponse)
                {
                    Mediator.Log("Provide player's name:");
                    var name = Mediator.Recieve();
                    Mediator.Log("Is dealer?:");
                    var dealerResponse = Convert.ToBoolean(Mediator.Recieve());
                    var result = AddPlayer(name, dealerResponse);
                    if (!result.Success)
                    {
                        Mediator.Log(result.Message);
                    }
                }

                Mediator.Log("Do you want to remove a player?");
                var removePlayerResponse = Convert.ToBoolean(Mediator.Recieve());
                if (removePlayerResponse)
                {
                    Mediator.Log("Provide player's name:", Players.Select(t => t.Name).ToList());
                    var name = Mediator.Recieve();
                    var result = RemovePlayer(name);
                    if (!result.Success)
                    {
                        Mediator.Log(result.Message);
                    }
                }

                if (!addPlayerResponse && !removePlayerResponse)
                {
                    break;
                }
            }

            return new OperationResult() {Success = true};
        }

        private string GetPlayerCards(Dictionary<AbstractPlayer, List<Card>> playersCards, AbstractPlayer player)
        {
            var playerCards = new StringBuilder();

            foreach (var card in playersCards[player])
            {
                playerCards.Append(card.Number + " " + card.Suit + ", ");
            }
            playerCards.Remove(playerCards.Length - 2, 2);

            return playerCards.ToString();
        }

        private void GetPlayerScores(Dictionary<AbstractPlayer, List<Card>> playersCards,
            Dictionary<AbstractPlayer, int> playersScores, AbstractPlayer player)
        {
            playersScores[player] = 0;
            foreach (var card in playersCards[player])
            {
                playersScores[player] += Scorer.GetScore(card);
            }
        }

        public OperationResult AddPlayer(string name, bool isDealer)
        {
            if (IsGameLocked)
            {
                return new OperationResult()
                {
                    Success = false,
                    Message = "You can not add players while the round is played."
                };
            }

            if (Players.Exists(t => t.IsDealer) && isDealer)
            {
                return new OperationResult() {Success = false, Message = "You can not add more then 1 dealer."};
            }

            Players.Add(new Player() {Name = name, IsDealer = isDealer});
            Mediator.Log($"Player {name} joined the game.");

            return new OperationResult() {Success = true};
        }

        public OperationResult RemovePlayer(string name)
        {
            if (IsGameLocked)
            {
                return new OperationResult()
                {
                    Success = false,
                    Message = "You can not remove players while the round is played."
                };
            }
            if (Players.First(t => t.Name.ToLower() == name.ToLower()).IsDealer)
            {
                return new OperationResult() {Success = false, Message = "You can not remove a dealer."};
            }

            Players.Remove(Players.First(t => t.Name.ToLower() == name.ToLower()));
            Mediator.Log($"Player {name} left the game.");

            return new OperationResult() {Success = true};
        }

        public BlackJackGame(List<AbstractPlayer> players)
        {
            RoundStats = new Dictionary<int, List<Statistics>>();
            Players = players;
            Scorer = new ScorerFactory().CreateScorer();
            Mediator = new ConsoleMediatorFactory().CreateConsoleMediator();
        }
    }
}