﻿using App.Entities.BlackJack;

namespace App.Logic.BlackJack
{
    public class Statistics
    {
        public AbstractPlayer Player { get; set; }

        public int Wins { get; set; }

        public int Losses { get; set; }

        public int Total { get; set; }

        public string Get()
        {
            return $"Player {Player.Name} stats: Wins: {Wins}, Losses: {Losses}, Total: {Total}. \n";
        }
    }
}
