﻿using System;

namespace App.Logic.Util
{
    public class OperationResult
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public Exception Exception { get; set; }
    }
}
