﻿using App.Entities.BlackJack;

namespace App.Factories.BlackJack
{
    public class PlayersFactory
    {
        public Player CreatePlayer(string name, bool isDealer)
        {
            return new Player() {IsDealer = isDealer, Name = name};
        }
    }
}
