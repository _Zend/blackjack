﻿using App.Entities.BlackJack;

namespace App.Factories.BlackJack
{
    public class CardFactory
    {
        public Card CreateCard()
        {
            return new Card();
        }
    }
}
