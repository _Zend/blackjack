﻿using App.Entities.BlackJack;

namespace App.Factories.BlackJack
{
    public class DeckFactory
    {
        public Deck CreateDeck()
        {
            return new Deck();
        }
    }
}
