﻿using App.Entities.Util;

namespace App.Factories.Util
{
    public class ConsoleMediatorFactory
    {
        public ConsoleMediator CreateConsoleMediator()
        {
            return new ConsoleMediator();
        }
    }
}
