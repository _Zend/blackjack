﻿using App.Entities.Util;

namespace App.Factories.Util
{
    public class ScorerFactory
    {
        public Scorer CreateScorer()
        {
            return new Scorer();
        }
    }
}
