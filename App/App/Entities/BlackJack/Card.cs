﻿namespace App.Entities.BlackJack
{
    public class Card
    {
        public Number Number { get; set; }

        public Suit Suit { get; set; }
    }

    public enum Suit
    {
        Heart = 1,
        Diamond,
        Club,
        Spade
    }

    public enum Number
    {
        Ace = 1,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King
    }
}
