﻿namespace App.Entities.BlackJack
{
    public abstract class AbstractPlayer
    {
        public string Name { get; set; }
        public bool IsDealer { get; set; }
    }
}
