﻿using System;
using System.Collections.Generic;
using App.Factories.BlackJack;

namespace App.Entities.BlackJack
{
    public class Deck
    {
        public List<Card> Cards { get; set; } = new List<Card>();

        public Card GetRandomCard()
        {
            var index = new Random().Next(Cards.Count);
            var card = Cards[index];
            Cards.RemoveAt(index);

            return card;
        }

        public Deck()
        {
            foreach (var number in Enum.GetValues(typeof(Number)))
            {
                foreach (var suit in Enum.GetValues(typeof(Suit)))
                {
                    var card = new CardFactory().CreateCard();
                    card.Number = (Number) number;
                    card.Suit = (Suit) suit;
                    Cards.Add(card);
                }
            }
        }
    }
}
