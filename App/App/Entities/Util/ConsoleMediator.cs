﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Interfaces.Util;

namespace App.Entities.Util
{
    public class ConsoleMediator : IMediator
    {
        public string Recieve()
        {
            return Console.ReadLine();
        }

        public void Log(string message)
        {
            Console.WriteLine(message + "\n");
        }
        public void Log(string message, List<string> parameters)
        {
            Console.WriteLine(message + "\n");
            foreach (var parameter in parameters)
            {
                if (parameters.Last() == parameter)
                {
                    Console.WriteLine(parameter);
                    return;
                }
                Console.WriteLine(parameter + ", ");
            }
        }
    }
}
