﻿using App.Entities.BlackJack;
using App.Interfaces.Util;

namespace App.Entities.Util
{
    public class Scorer : IScorer
    {
        private const int ScoreForImages = 10;

        private const int ScoreForAce = 11;

        public int GetScore(Card card)
        {
            var tenPointsScoreCondition = (card.Number == Number.Ten) || (card.Number == Number.King)
                                          || (card.Number == Number.Queen) || (card.Number == Number.Jack);
            if (tenPointsScoreCondition)
            {
                return ScoreForImages;
            }
            if (card.Number == Number.Ace)
            {
                return ScoreForAce;
            }
            return (int) card.Number;
        }
    }
}
