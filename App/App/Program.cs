﻿using System.Collections.Generic;
using App.Entities.BlackJack;
using App.Factories.BlackJack;
using App.Logic.BlackJack;

namespace App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var playersFactory = new PlayersFactory();
            var players = new List<AbstractPlayer>()
            {
                playersFactory.CreatePlayer("John", true),
                playersFactory.CreatePlayer("David", false),
                playersFactory.CreatePlayer("Bob", false)
            };
            var game = new BlackJackGame(players);
            game.StartGame();
        }
    }
}
