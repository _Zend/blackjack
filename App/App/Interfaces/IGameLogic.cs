﻿using App.Logic.Util;

namespace App.Interfaces
{
    public interface IGameLogic
    {
        OperationResult StartGame();
        OperationResult FinishGame();
        OperationResult StartRound();
        OperationResult FinishRound();
    }
}
