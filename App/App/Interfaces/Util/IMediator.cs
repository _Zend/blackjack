﻿using System.Collections.Generic;

namespace App.Interfaces.Util
{
    public interface IMediator
    {
        void Log(string message);

        void Log(string message, List<string> parameters);

        string Recieve();
    }
}
