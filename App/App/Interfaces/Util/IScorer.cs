﻿using App.Entities.BlackJack;

namespace App.Interfaces.Util
{
    public interface IScorer
    {
        int GetScore(Card card);
    }
}
